package task08;

import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class WorkingHard {
    public static void main(String[] args) throws IOException {
        //String s = "BZh91AY&SYA\\xaf\\x82\\r\\x00\\x00\\x01\\x01\\x80\\x02\\xc0\\x02\\x00 \\x00!\\x9ah3M\\x07<]\\xc9\\x14\\xe1BA\\x06\\xbe\\x084";
        //String un = "src\\task8\\un.txt";

        //ByteArrayInputStream inputStream = new ByteArrayInputStream(s.getBytes("UTF-8"));
        //InputStream inputStream = new ByteArrayInputStream(s.getBytes("ISO-8859-1"));
        //byte[] bytes2 = IOUtils.toByteArray(inputStream);
        //System.out.println(bzIn.getBytesRead());

        // Init
        String fileName1 = "src\\task8\\un.txt"; // from bash printf ''
        String fileName2 = "src\\task8\\pw.txt"; // from bash printf ''
        String fileNameOut1 = "src\\task8\\archive1.txt";
        String fileNameOut2 = "src\\task8\\archive2.txt";

        // BZip2
        BZipFromFile (fileName1, fileNameOut1);
        BZipFromFile (fileName2, fileNameOut2);
    }
    public static void BZipFromFile (String fileNameIn, String fileNameOut) throws IOException {
        FileOutputStream out = new FileOutputStream(fileNameOut);

        FileInputStream in = new FileInputStream(fileNameIn);
        BZip2CompressorInputStream bzIn = new BZip2CompressorInputStream(in);

        byte[] buffer = new byte[100];
        int n;
        while (-1 != (n = bzIn.read(buffer))) {
            out.write(buffer, 0, n);
        }
        bzIn.close();
        out.close();
    }
}
