package task21;

import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;

import java.io.*;
import java.util.zip.InflaterInputStream;

public class Pack {
    public static void main(String[] args) throws Exception {

        //FileInputStream fis = new FileInputStream("src\\task21\\original.txt");
        //FileOutputStream fos = new FileOutputStream("src\\task21\\deflated.txt");
        //DeflaterOutputStream dos = new DeflaterOutputStream(fos);
        //doCopy(fis, dos); // copy original.txt to deflated.txt and compress it

        FileInputStream fis2 = new FileInputStream("src\\task21\\package.pack");
        //FileOutputStream fos2 = new FileOutputStream("src\\task21\\package_out.pack");
        //InflaterInputStream iis = new InflaterInputStream(fis2);

        // Get bytes from first file
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        int byte1;
        while((byte1 = fis2.read()) != -1) {
            baos.write(byte1);
        }
        baos.close();
        byte[] bytes = baos.toByteArray();

        // Start loop
        boolean stop = false;
        while(!stop) {
            InputStream is = new ByteArrayInputStream(bytes);

            StringBuilder strHex = new StringBuilder(bytesToHex(bytes, 0, 2));
            //System.out.print(strHex);
            if (strHex.toString().equals("789c")) {
                bytes = ZLIB(is);
                System.out.print("  ");
            }
            else if (strHex.toString().equals("425a")) {
                bytes = BZip2(is);
                System.out.print("9c");
            }
            else if (bytesToHex(bytes, bytes.length-2, 2).equals("9c78")) {
                bytes = reverseArray(bytes);
                System.out.println();
            }
            else {
                stop = true;
                //System.out.println(new String(bytes));
            }

            is.close();
        }
    }

    public static byte[] ZLIB(InputStream is) throws IOException {
        InflaterInputStream iis2 = new InflaterInputStream(is);

        // Write to Byte Array Output Stream
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        int oneByte;
        while ((oneByte = iis2.read()) != -1) {
            baos.write(oneByte);
        }
        baos.close();

        iis2.close();

        return baos.toByteArray();
    }

    public static byte[] BZip2(InputStream is) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        int byte1;
        BZip2CompressorInputStream bzIn = new BZip2CompressorInputStream(is);
        while ((byte1 = bzIn.read()) != -1) {
            baos.write(byte1);
        }
        bzIn.close();
        baos.close();

        return baos.toByteArray();
    }

    public static byte[] reverseArray(byte[] bytes) {
        byte tmp;
        for(int i=0; i < (bytes.length/2); i++) {
            tmp = bytes[i];
            bytes[i] = bytes[bytes.length - i - 1];
            bytes[bytes.length - i - 1] = tmp;
        }
        return bytes;
    }

    public static String bytesToHex(byte[] in, int start, int n) {
        final StringBuilder str = new StringBuilder();
        for (int i = start; i < start+n; i++) {
            str.append(String.format("%02x", in[i]));
        }
        return str.toString();
    }

    /*public static void doCopy(InputStream is, OutputStream os) throws Exception {
        int oneByte;
        while ((oneByte = is.read()) != -1) {
            os.write(oneByte);
        }
        os.close();
        is.close();
    }*/
}
