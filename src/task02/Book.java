package task02;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class Book {
    public static void main(String[] args) throws IOException {
        FileInputStream fstream = new FileInputStream("src\\task2\\text.txt");
        BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
        String s;

        char ch;
        //HashMap<Character, Integer> map = new HashMap<Character, Integer>();
        LinkedHashMap<Character, Integer> map = new LinkedHashMap<>();
        while ((s = br.readLine()) != null) {
            for (int i = 0; i < s.length(); i++) {
                ch = s.charAt(i);
                if (map.containsKey(ch))
                    map.put(ch, (map.get(ch) + 1));
                else
                    map.put(ch, 1);
            }
        }
        br.close();

        for (Map.Entry entry : map.entrySet()) {
            if (entry.getValue().equals(1))
                System.out.print(entry.getKey());
            //System.out.println("Key: " + entry.getKey() + " Value: " + entry.getValue());
        }
   }
}
