package task10;

import java.util.LinkedList;

public class Bull {
    public static void main(String[] args) {
        LinkedList<String> list = new LinkedList<String>();
        list.add("1");

        StringBuilder s = new StringBuilder();
        String prev_number;
        int cnt;
        int cur_num;
        int num;
        for (int i = 1; i < 31; i++) {
            s.setLength(0);
            cnt = 0;
            prev_number = list.get(i-1);
            num = Character.getNumericValue(prev_number.charAt(0));

            for (char ch : prev_number.toCharArray()) {
                cur_num = Character.getNumericValue(ch);
                if (cur_num != num) {
                    s.append(cnt);
                    s.append(num);
                    cnt = 1;
                    num = cur_num;
                }
                else
                    cnt++;
            }
            s.append(cnt);
            s.append(num);
            list.add(s.toString());

        }
        for (int i = 0; i < list.size(); i++) {
            System.out.println("i: " + i + " = " + list.get(i));
        }
        System.out.println("length string #30 = " + list.get(30).length());
    }
}
