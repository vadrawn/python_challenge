package task17;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;

import static task04.Site.openNextUrl;
import static task08.WorkingHard.BZipFromFile;

public class Eat {
    public static void main(String[] args) throws IOException {

        // ========== PART 1 ========== //
        // import task 4
        String url = "http://www.pythonchallenge.com/pc/def/linkedlist.php?busynothing=";
        String fileNameOut = "src\\task17\\output.txt";
        BufferedWriter outFile = new BufferedWriter(new FileWriter(fileNameOut));
        BufferedWriter outFileWOdecode = new BufferedWriter(new FileWriter("src\\task17\\outputWOdecode.txt"));
        String num = "12345";
        String nx_num;
        for (int i = 0; i < 118; i++) {
            System.out.print(i + ": " + num + " ") ;
            // get cookie
            URLConnection connection = new URL(url + num).openConnection();
            String cookies = String.valueOf(connection.getHeaderFields().get("Set-Cookie"));
            String sFromCook = cookies.substring(cookies.indexOf("=") + 1, cookies.indexOf(";"));
            String uncodeS = java.net.URLDecoder.decode(sFromCook, "UTF-16");
            outFile.append(uncodeS);
            System.out.println("symb: " + uncodeS + " ");

            // write uncode text to file
            outFileWOdecode.append(sFromCook);

            // get next number
            nx_num = openNextUrl(url + num);
            num = nx_num;
        }
        outFile.close();
        outFileWOdecode.close();


        // ========== PART 2 ========== //
        // import task 8
        BZipFromFile ("src\\task17\\from_bash.txt", "src\\task17\\result.txt");


        // ========== PART 3 ========== //
        // Set cookie and read content url page
        URLConnection connection = new URL("http://www.pythonchallenge.com/pc/stuff/violin.php").openConnection();
        connection.setRequestProperty("Cookie", "info=the flowers are on their way");

        StringBuilder content = new StringBuilder();
        String line;
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        while ((line = bufferedReader.readLine()) != null)
            content.append(line + "\n");
        bufferedReader.close();
        System.out.println(content);


        /*
        Map<String, List<String>> cookies = connection.getHeaderFields();
        for (Map.Entry entry : cookies.entrySet()) {
            System.out.println("Key: " + entry.getKey() + ", Val: " + entry.getValue());
        }
        */
    }
}
