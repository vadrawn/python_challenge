package task15;

import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.GregorianCalendar;

public class Uzi {

    public static void main(String[] args) throws ParseException, IOException {
        Date date = new Date();
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(date);

        for (int year = 1; year < 2017; year++) {
            calendar.set(year, 0, 26);
            int dotw = calendar.get(GregorianCalendar.DAY_OF_WEEK);
            // Понедельник, год начинается на 1 и заканчивается на 6
            if(dotw == GregorianCalendar.MONDAY & (year%10 == 6) & (year/100 == 1 | year/1000 == 1)) {
                // Только високосный
                if (year%4==0 || (year%400==0 && year%100!=0))
                //if (calendar.isLeapYear(year))
                    System.out.println("Year: " + year);
            }
        }
        /*
        BufferedWriter outFile = new BufferedWriter(new FileWriter("src\\task15\\year.txt"));
        for (int year = 1; year <= 2017; year++) {
            calendar.set(year, 0, 1);
            //jan1 = calendar.get(GregorianCalendar.DAY_OF_WEEK);
            String dayOfWeek = calendar.getDisplayName( Calendar.DAY_OF_WEEK ,Calendar.LONG, Locale.ENGLISH);
            System.out.println(year + ": " + dayOfWeek + " " + calendar.get(GregorianCalendar.DAY_OF_WEEK));
            outFile.append(year + ": " + dayOfWeek);
            outFile.append("\r\n");
        }
        outFile.close();
        */
    }
}
