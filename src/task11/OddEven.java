package task11;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class OddEven {
    public static void main(String[] args) throws IOException {
        BufferedImage png = ImageIO.read(new File("src\\task11\\cave.jpg"));
        int[][] array = new int[640][480];
        for (int x = 0; x < 640;) {
            for (int y = 0; y < 480;) {
                array[x][y] = png.getRGB(x, y);
                y = y+2;
            }
            x = x+2;
        }
        for (int x = 0; x < 640; x++) {
            for (int y = 0; y < 480; y++) {
                png.setRGB(x, y, array[x][y]);
            }
        }
        ImageIO.write(png, "png", new File("src\\task11\\cave2.png"));
    }
}
