package task14;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Italy {
    public static void main(String[] args) throws IOException {
        BufferedImage wire = ImageIO.read(new File("src\\task14\\wire.png"));
        BufferedImage wire2 = new BufferedImage(100, 100, BufferedImage.TYPE_INT_RGB);
        int p = 0;
        for (int i = 0; i < 50; i++) {
            for (int x = i; x < 100-i; x++) {
                wire2.setRGB(x, i, wire.getRGB(p,0));
                p++;
            }
            for (int y = 1+i; y < 100-i; y++) {
                wire2.setRGB(99-i, y, wire.getRGB(p,0));
                p++;
            }
            for (int x = 98-i; x >= i; x--) {
                wire2.setRGB(x,99-i, wire.getRGB(p,0));
                p++;
            }
            for (int y = 98-i; y >= 1+i; y--) {
                wire2.setRGB(i, y, wire.getRGB(p,0));
                p++;
            }
        }
        ImageIO.write(wire2, "png", new File("src\\task14\\wire2.png"));
    }
}