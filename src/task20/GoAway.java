package task20;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.*;
import java.util.Arrays;

public class GoAway {
    public static void main(String[] args) throws IOException {
        URLConnection connection = new URL("http://www.pythonchallenge.com/pc/hex/idiot2.html").openConnection();

        Authenticator.setDefault (new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication ("butter", "fly".toCharArray());
            }
        });
        System.out.println("Header: " + connection.getHeaderFields());

        // New connection
        URL url = new URL("http://www.pythonchallenge.com/pc/hex/unreal.jpg");

        System.out.println("Sending get request: "+ url);
        int startByte = 30203;
        byte[] bytes;
        for (int i = 0; i < 5; i++) {
            // Get content
            bytes = getUrlContent(url, ""+startByte, "2123456789");

            String s = new String (bytes);
            System.out.println(s);

            startByte = startByte + bytes.length;
        }

        byte[] bytes2;
        String s;

        // 2
        bytes2 = getUrlContent(url, ""+2123456713, "2123456789");
        s = new String(bytes2);
        System.out.println(s);

        // 3
        bytes2 = getUrlContent(url, ""+(2123456713+bytes2.length), "2123456789");
        s = new String(bytes2);
        System.out.println(s);

        // 4
        String name1 = "src\\task20\\readme.zip";
        FileOutputStream os = new FileOutputStream(new File(name1));

        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestProperty("Range", "bytes=1152983631-1153223363");
        System.out.println("Range: bytes=1152983631-1153223363");

        InputStream is = con.getInputStream();
        int i;
        while ((i = is.read()) != -1) {
            os.write(i);
        }
        os.close();
        con.disconnect();
    }
    static byte[] getUrlContent (URL url, String startByte, String endByte) throws IOException {
        HttpURLConnection con = (HttpURLConnection) url.openConnection();

        con.setRequestProperty("Range", "bytes="+startByte+"-"+endByte);
        System.out.println("Range: bytes="+startByte+"-"+endByte);

        InputStream is = con.getInputStream();

        int availableBytes = is.available();
        System.out.println(availableBytes);

        byte[] bytes = new byte[availableBytes];
        is.read(bytes);
        System.out.println(Arrays.toString(bytes));

        con.disconnect();

        return bytes;
    }
}
