package task06;

import java.io.*;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class Zip {
    public static void main(String[] args) throws Exception {
        String comment2 = "";
        // Method1: ZipFile
        String zipname = "src\\task6\\channel.zip";
        ZipFile zip = new ZipFile(zipname);
        Enumeration entries = zip.entries();
        String name_in_zip;
        String comment_in_zip;
        HashMap<String, String> map = new HashMap<>();

        while (entries.hasMoreElements()) {
            ZipEntry entry = (ZipEntry) entries.nextElement();
            name_in_zip = entry.getName();
            comment_in_zip = entry.getComment();
            map.put(name_in_zip, comment_in_zip);
            System.out.printf("Название: %s \t комментарий: %s !\n", name_in_zip, comment_in_zip);
        }

        String filename = "src\\task6\\channel\\";
        String n = "90052.txt";
        String line;
        boolean end = false;
        while (!end) {
            System.out.println("Open file: " + filename + n + ".txt");
            BufferedReader reader = new BufferedReader(new FileReader(filename + n));
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
                if (line.equals("Collect the comments."))
                    end = true;
                Pattern p = Pattern.compile("[0-9]+");
                Matcher m = p.matcher(line);
                while (m.find())
                    n = line.substring(m.start(), m.end()) + ".txt";
                comment2 = comment2 + map.get(n);
            }
            reader.close();
        }
        System.out.print("Comments: " + comment2);
    }
}
