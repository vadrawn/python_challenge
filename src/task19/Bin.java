package task19;

import java.io.*;
import java.util.Arrays;
import java.util.Base64;

public class Bin {
    public static void main(String[] args) throws IOException {
        FileInputStream fstream = new FileInputStream("src\\task19\\base64.txt");
        BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
        String str = br.readLine();
        byte[] decoded = Base64.getDecoder().decode(str);
        br.close();
        System.out.println(Arrays.toString(decoded));

        String name1 = "src\\task19\\indian2.wav";
        File file = new File(name1);
        FileOutputStream os = new FileOutputStream(file);

        for (int i = 0; i < decoded.length; i++) {
            if (i <= 44)
                os.write(decoded[i]);
            else if (i%2 == 0)
                os.write(decoded[i]);
        }
        os.close();
    }
}
