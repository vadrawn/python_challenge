package task18;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

import static org.apache.commons.compress.utils.Charsets.ISO_8859_1;

public class Balloons {
    public static void main(String[] args) throws IOException {
        ArrayList<String> file1 = new ArrayList<String>();
        file1 = fileToList("src\\task18\\delta1.txt");
        ArrayList<String> file2 = new ArrayList<String>();
        file2 = fileToList("src\\task18\\delta2.txt");

        String imgName1 = "src\\task18\\img1.png";
        String imgName2 = "src\\task18\\img2.png";
        String imgName3 = "src\\task18\\img3.png";

        String s1, s2;
        for (int i = 0; i < file1.size(); i++) {
            s1 = file1.get(i).trim(); // trim() - удаляет пробелы в начале и конце строки
            s2 = file2.get(i).trim();
            if (s1.equals(s2)) {
                writeStringToFile(s1, imgName1);
                System.out.println("str: " + (i+1) + " equals");
            }
            else if (file1.get(i+1).equals(s2)) {
                writeStringToFile(s1, imgName2);
                file2.add(i, s1);
                System.out.println("str: " + (i+1) + " write to img2");
            }
            else if (s1.equals(file2.get(i+1))) {
                writeStringToFile(s2, imgName3);
                file1.add(i, s2);
                System.out.println("str: " + (i+1) + " write to img3");
            }
            else {
                writeStringToFile(s1, imgName2);
                writeStringToFile(s2, imgName3);
                System.out.println("str: " + (i+1) + " write to img2 and img3");
            }
        }
    }

    static ArrayList<String> fileToList (String fileName) throws IOException {
        FileInputStream fstream = new FileInputStream(fileName);
        BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
        String str;
        ArrayList<String> listFile = new ArrayList<String>();
        while ((str = br.readLine()) != null)
            listFile.add(str);
        br.close();
        return listFile;
    }

    static void writeStringToFile (String str, String fileName) throws IOException {
        boolean append = new File(fileName).exists();
        Writer writer = new OutputStreamWriter(new FileOutputStream(fileName, append), ISO_8859_1);

        Scanner sc = new Scanner(str);
        String s;
        while (sc.hasNext()) {
            s = sc.next();
            if (s.equals(""))
                continue;
            writer.write(Integer.parseInt(s, 16));
        }
        writer.close();
        sc.close();
    }
}