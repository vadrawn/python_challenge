package task12;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class DealingEvil {
    public static void main(String[] args) throws IOException {
        Path path = Paths.get("src\\task12\\evil2.gfx");
        FileOutputStream img1 = new FileOutputStream("src\\task12\\new1.png");
        FileOutputStream img2 = new FileOutputStream("src\\task12\\new2.png");
        FileOutputStream img3 = new FileOutputStream("src\\task12\\new3.png");
        FileOutputStream img4 = new FileOutputStream("src\\task12\\new4.png");
        FileOutputStream img5 = new FileOutputStream("src\\task12\\new5.png");
        byte[] data = Files.readAllBytes(path);

        for (int i = 0; i < data.length; i+=5) {
            img1.write(data[i]);
            img2.write(data[i+1]);
            img3.write(data[i+2]);
            img4.write(data[i+3]);
            img5.write(data[i+4]);
        }
        img1.close();
        img2.close();
        img3.close();
        img4.close();
        img5.close();
    }
}
