package task13;

import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Vector;

public class Phonebook {
    private final static String server_url = "http://www.pythonchallenge.com/pc/phonebook.php";

    public static void main (String [] args) throws MalformedURLException, XmlRpcException {
        XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
        config.setServerURL(new URL(server_url));
        XmlRpcClient client = new XmlRpcClient();
        client.setConfig(config);

        //System.out.println(client.getConfig().toString());

        Vector param = new Vector();
        //param.add("Bert");
        param.add("Leopold");
        Object result = client.execute("phone", param);
        System.out.println(result.toString());
    }
}
