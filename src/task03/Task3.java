package task03;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Task3 {
    public static void main(String[] args) throws IOException {
        FileInputStream fstream = new FileInputStream("src\\task3\\text.txt");
        BufferedReader br = new BufferedReader(new InputStreamReader(fstream));

        String s;
        while ((s = br.readLine()) != null) {
            Pattern p = Pattern.compile("[a-z][A-Z]{3}[a-z][A-Z]{3}[a-z]");
            Matcher m = p.matcher(s);
            while(m.find())
                System.out.print(s.substring(m.start()+4, m.end()-4));
        }
        br.close();
    }
}
