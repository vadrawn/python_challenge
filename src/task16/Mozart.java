package task16;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Mozart {
    public static void main(String[] args) throws IOException {
        BufferedImage png = ImageIO.read(new File("src\\task16\\mozart.gif"));

        int[][] array = new int[640][480];
        for (int x = 0; x < 640; x++) {
            for (int y = 0; y < 480; y++) {
                array[x][y] = png.getRGB(x, y);
            }
        }

        BufferedImage img2 = new BufferedImage(640, 480, BufferedImage.TYPE_INT_RGB);
        Color match = new Color(255, 0, 255);
        for (int y = 0; y < 480; y++) {
            for (int x = 0; x < 640; x++) {
                if (png.getRGB(x, y) == match.getRGB()) {
                    for (int x2 = 0; x2 < 640; x2++) {
                        if (x+x2 < 640)
                            img2.setRGB(x2, y, png.getRGB(x+x2, y));
                    }
                }
            }
        }
        ImageIO.write(img2, "png", new File("src\\task16\\mozart.png"));
/*
        int cnt;
        int cnt2 = 0;
        for (int y = 0; y < 480; y++) {
            cnt = 0;
            for (int x = 0; x < 640; x++) {
                if (png.getRGB(x, y) == match.getRGB()) {
                    img2.setRGB(x, y, png.getRGB(x, y));
                    cnt2++;
                    if (png.getRGB(x, y) != png.getRGB(x+1, y))
                        cnt++;
                }
            }
            System.out.println("y: " + y + ", cnt: " + cnt);
        }
        System.out.println(cnt2);
        ImageIO.write(img2, "png", new File("src\\task16\\mozart.png"));
*/

    }
}
