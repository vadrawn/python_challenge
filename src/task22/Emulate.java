package task22;

import com.sun.imageio.plugins.gif.GIFImageReader;
import com.sun.imageio.plugins.gif.GIFImageReaderSpi;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.FileImageOutputStream;
import javax.imageio.stream.ImageOutputStream;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class Emulate {
    public static void main(String[] args) throws Exception {

        // Get Frames from Gif
        ArrayList<BufferedImage> frames = new ArrayList<BufferedImage>();
        frames = getFrames(new File("src\\task22\\white.gif"));

        // Edit frames
        Color match = new Color(8, 8, 8);
        Color white = new Color(255, 255, 255);
        Color black = new Color(0,0,0);
        Color red = new Color(255,0,0);

        ArrayList<Integer> coorX = new ArrayList<Integer>();
        ArrayList<Integer> coorY = new ArrayList<Integer>();

        int j = 0;
        for(BufferedImage bi : frames) {
            for (int y = 0; y < bi.getHeight(); y++) {
                for (int x = 0; x < bi.getWidth(); x++) {
                    if (bi.getRGB(x, y) == match.getRGB()) {
                        int x_ = getDir(x);
                        int y_ = getDir(y);
                        //bi.setRGB(x+x_,y+y_, white.getRGB());
                        //ImageIO.write(bi, "gif", new File("src\\task22\\frames2\\frame"+ (j++) +".gif"));
                        coorX.add(x_);
                        coorY.add(y_);
                        System.out.println("x: " + x + ", y: " + y + " (" + x_ + "," + y_ + ")");
                    }
                }
            }
            //System.out.println(bi);
        }

        ArrayList<BufferedImage> frames2 = new ArrayList<BufferedImage>();
        BufferedImage bi2 = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);

        int curX = 100;
        int curY = 100;
        for (int i = 0; i < coorX.size(); i++) {
            curX += coorX.get(i);
            curY += coorY.get(i);
            bi2.setRGB(curX, curY, red.getRGB());
            frames2.add(bi2);
            if (coorX.get(i) == 0 && coorY.get(i) == 0) {
                //bi2 = frames.get(0);
                //ImageIO.write(bi2, "gif", new File("src\\task22\\frames\\frame" + i + ".gif"));
                bi2 = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
            }
        }

        // Make animated gif
        makeAnimatedGIF(frames2, new File("src\\task22\\white_out.gif"));
    }

    public static ArrayList<BufferedImage> getFrames(File gif) throws IOException {
        ArrayList<BufferedImage> frames = new ArrayList<BufferedImage>();
        BufferedImage master = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);

        ImageReader ir = new GIFImageReader(new GIFImageReaderSpi());
        ir.setInput(ImageIO.createImageInputStream(gif));
        for (int i = 0; i < ir.getNumImages(true); i++) {
            frames.add(new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB));
            master.getGraphics().drawImage(ir.read(i), 0, 0, null);
            frames.get(i).setData(master.getData());
        }
        return frames;
    }

    public static void makeAnimatedGIF(ArrayList<BufferedImage> frames, File file) throws IOException {
        ImageOutputStream output = new FileImageOutputStream(file);
        GifSequenceWriter writer = new GifSequenceWriter(output, frames.get(2).getType(), 10, false);
        for(BufferedImage bi : frames)
            writer.writeToSequence(bi);

        writer.close();
        output.close();
    }

    public static int getDir(int coord){
        if (coord == 98) return -2;
        else if (coord == 100) return 0;
        else if (coord == 102) return 2;
        else return 0;
    }
}
