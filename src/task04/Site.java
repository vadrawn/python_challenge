package task04;

import org.jsoup.Jsoup;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Site {
    public static void main(String[] args) throws IOException {
        String url = "http://www.pythonchallenge.com/pc/def/linkedlist.php?nothing=";

        String num = "12345";
        String nx_num;
        for (int i = 0; i < 250; i++) {
            if (i == 85)
                num = Integer.toString(Integer.parseInt(num)/2);
            System.out.print(i + ": " + num + " ") ;
            nx_num = openNextUrl(url + num);
            num = nx_num;
        }
    }
    public static String openNextUrl(String url) throws IOException {
        String s;
        String num = "";
        // get string on site
        org.jsoup.nodes.Document doc  = Jsoup.connect(url).get();
        s = doc.text();
        System.out.println("(" + s + ")");

        // find num
        Pattern p = Pattern.compile("[0-9]+");
        Matcher m = p.matcher(s);
        while(m.find())
            num = s.substring(m.start(), m.end());

        return num;
    }
}
