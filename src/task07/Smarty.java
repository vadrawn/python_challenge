package task07;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Smarty {
    public static void main(String[] args) throws IOException {
        // GET Color
        String filename = "src\\task7\\oxygen.png";
        File file= new File(filename);
        BufferedImage image = ImageIO.read(file);

        StringBuilder new_s = new StringBuilder();
        char ascii = ' ';
        for (int i = 0; i < 88; i++) {
            Color color = new Color(image.getRGB(7*i, 47));
            ascii = (char)color.getRed();
            new_s.append(ascii);
        }
        System.out.println("String in image: " + new_s);

        // Parse string
        LinkedList<Integer> list = new LinkedList<>();
        Pattern p = Pattern.compile("\\d+");
        Matcher m = p.matcher(new_s);
        while(m.find())
            list.add(Integer.parseInt(new_s.substring(m.start(), m.end())));

        // OUT
        System.out.print("Answer: ");
        for (int num : list)
            System.out.print((char) num);
    }
}
